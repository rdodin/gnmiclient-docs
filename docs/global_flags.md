### address
The address flag `[-a | --address]` is used to specify the target's gNMI server address in address:port format, for e.g: `192.168.113.11:57400`

Multiple target addresses can be specified, either as comma separated values:
```
gnmic --address 192.168.113.11:57400,192.168.113.12:57400 
```
or by using the `--address` flag multiple times:
```
gnmic -a 192.168.113.11:57400 --address 192.168.113.12:57400
```

### config
The `--config` flag specifies the location of a configuration file that `gnmic` will read. Defaults to `$HOME/.gnmic.yaml`.

### debug
The debug flag `[-d | --debug]` enables the printing of extra information when sending/receiving an RPC

### encoding
The encoding flag `[-e | --encoding]` is used to specify the [gNMI encoding](https://github.com/openconfig/reference/blob/master/rpc/gnmi/gnmi-specification.md#23-structured-data-types) of the Update part of a [Notification](https://github.com/openconfig/reference/blob/master/rpc/gnmi/gnmi-specification.md#21-reusable-notification-message-format) message.

It is case insensitive and must be one of: JSON, BYTES, PROTO, ASCII, JSON_IETF

### insecure
The insecure flag `[--insecure]` is used to indicate that the client wishes to establish an non-TLS enabled gRPC connection.

To disable certificate validation in a TLS-enabled connection use [`skip-verify`](#skip-verify) flag.

### log-file
The log-file flag `[--log-file <path>]` changes the log output from the default `stderr` location to a file referenced by the path.

### no-prefix
The no prefix flag `[--no-prefix]` disables prefixing the json formatted responses with `[ip:port]` string.

Note that in case a single target is specified, the prefix is not added.

### password
The password flag `[-p | --password]` is used to specify the target password as part of the user credentials. If omitted, the password input prompt is used to provide the password.

Note that in case multiple targets are used, all should use the same credentials.

### proxy-from-env
The proxy-from-env flag `[--proxy-from-env]` indicates that the gnmic should use the HTTP/HTTPS proxy addresses defined in the environment variables `http_proxy` and `https_proxy` to reach the targets specified using the `--address` flag.

### skip-verify
The skip verify flag `[--skip-verify]` indicates that the target should skip the signature verification steps, in case a secure connection is used.  

### timeout
The timeout flag `[--timeout]` specifies the gRPC timeout after which the connection attempt fails.

Valid formats: 10s, 1m30s, 1h.  Defaults to 10s

### tls-ca
The TLS CA flag `[--tls-ca]` specifies the root certificates for verifying server certificates encoded in PEM format.

### tls-cert
The tls cert flag `[--tls-cert]` specifies the public key for the client encoded in PEM format

### tls-key
The tls key flag `[--tls-key]` specifies the private key for the client encoded in PEM format

### username
The username flag `[-u | --username]` is used to specify the target username as part of the user credentials. If omitted, the input prompt is used to provide the username.
