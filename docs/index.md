<nbsp/>
<p style="text-align:center;">![headline](images/gnmi-headline-1.svg)</p>
[gNMI](https://github.com/openconfig/reference/blob/master/rpc/gnmi/gnmi-specification.md) RPC capabilities, get, set, subscribe from the terminal.

### Usage
```
$ gnmic --help
run gnmi rpcs from the terminal

Usage:
  gnmic [command]

Available Commands:
  capabilities query targets gnmi capabilities
  get          run gnmi get on targets
  help         Help about any command
  path         generate gnmi or xpath style from yang file
  set          run gnmi set on targets
  subscribe    subscribe to gnmi updates on targets
  version      show gnmic version
```
### Global flags
```
  -a, --address strings    comma separated gnmi targets addresses
      --config string      config file (default is $HOME/gnmic.yaml)
  -d, --debug              debug mode
  -e, --encoding string    one of: JSON, BYTES, PROTO, ASCII, JSON_IETF. (default "JSON")
      --format string      output format, one of: [textproto, json] (default "json")
  -h, --help               help for gnmic
      --insecure           insecure connection
      --log-file string    log file path
      --max-msg-size int   max grpc msg size (default 536870912)
      --no-prefix          do not add [ip:port] prefix to print output in case of multiple targets
      --nolog              do not generate logs
  -p, --password string    password
      --proxy-from-env     use proxy from environment
      --skip-verify        skip verify tls connection
      --timeout string     grpc timeout (default "30s")
      --tls-ca string      tls certificate authority
      --tls-cert string    tls certificate
      --tls-key string     tls key
  -u, --username string    username
```
Refer to the [global flags](global_flags.md) page for a complete flags reference.